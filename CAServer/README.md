## Use Java
```
// 1. for Client
// build server
javac Server.java
// run local server
java Server

// 2. for Client
// build client
javac Client.java
// run client with local server
java Client
// or use remote server
SERVER_URL=... java Client
```

## Use Docker
```
// 1. for Server
// build
docker build . -t server
// run
docker run --rm -it --network=host server

// 2. for Client
// buld
docker build -f Client.Dockerfile . -t client
// run
docker run --rm -it --network=host client
```